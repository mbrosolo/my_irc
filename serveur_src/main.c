/*
** main.c for serveur in /home/brosol_m/docs/systeme_unix/my_ftp/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Mar 29 21:11:32 2012 matthias brosolo
** Last update Thu Mar 29 21:11:32 2012 matthias brosolo
*/

#include	"serveur.h"

int		get_sockfd(int sockfd)
{
  static int	_sockfd = 0;

  if (sockfd != -1)
    _sockfd = sockfd;
  return (_sockfd);
}

void		sighandler(int sig)
{
  (void) sig;
  close(get_sockfd(-1));
  exit(0);
}

int		main(int ac, char **av)
{
  int		sockfd;

  if (ac < 2)
    return (usage(av[0]));
  if ((sockfd = create_socket(av[1])) == EXIT_FAILURE)
    return (EXIT_FAILURE);
  signal(SIGINT, &sighandler);
  get_sockfd(sockfd);
  process_server(sockfd);
  return (EXIT_SUCCESS);
}
