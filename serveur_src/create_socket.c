/*
** create_socket.c for create_socket in /home/brosol_m/docs/systeme_unix/myftp-2015-2014s-brosol_m/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Apr  6 19:53:53 2012 matthias brosolo
** Last update Fri Apr  6 19:53:53 2012 matthias brosolo
*/

#include	"serveur.h"

int			create_socket(char *port)
{
  int			sockfd;
  struct protoent	*protocol;
  struct sockaddr_in	sock_addr;

  if (!(protocol = getprotobyname(TCP)))
    return (ret_error("getprotobyname()", NULL));
  if ((sockfd = socket(AF_INET, SOCK_STREAM, protocol->p_proto)) == -1)
    return (ret_error("socket()", NULL));
  sock_addr.sin_family = AF_INET;
  sock_addr.sin_port = htons(atoi(port));
  inet_aton("0.0.0.0", &sock_addr.sin_addr);
  if (bind(sockfd, (struct sockaddr *) &sock_addr, sizeof(sock_addr)) == -1)
    {
      close(sockfd);
      return (ret_error("bind()", NULL));
    }
  if (listen(sockfd, MAX_CONNECTION) == -1)
    {
      close(sockfd);
      return (ret_error("listen()", NULL));
    }
  return (sockfd);
}
