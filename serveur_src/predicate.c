/*
** predicate.c for predicate.c in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Apr 20 19:06:40 2012 matthias brosolo
** Last update Fri Apr 20 19:06:40 2012 matthias brosolo
*/

#include	"serveur.h"

enum e_bool	cmp_nickname(t_client *client, char *s)
{
  if (!strncmp(client->name, s, NAME_LEN))
    return (TRUE);
  return (FALSE);
}

enum e_bool	cmp_channame(t_chan *chan, char *s)
{
  if (!strncmp(chan->name, s, CHAN_NAME_LEN))
    return (TRUE);
  return (FALSE);
}
