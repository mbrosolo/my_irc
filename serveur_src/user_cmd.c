/*
** user_cmd.c for user_cmd in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 23:14:04 2012 matthias brosolo
** Last update Sun Apr 22 23:06:56 2012 anna texier
*/

#include	"serveur.h"

int	change_nickname(t_srv *srv, t_client *client, t_data *data)
{
  char		*name;
  t_pair_data	pair;

  (void) strtok(data->content, " \t");
  name = strtok(NULL, " \t");
  if (name && !srv->users_list.get(&srv->users_list, (t_pred) &cmp_nickname,
				   name))
    {
      make_pair_response(srv, &pair);
      pair.data.size = snprintf(pair.data.content, CONTENT_LEN,
				"%s is now known as %s", client->name, name);
      memset(data->name, 0, NAME_LEN);
      memset(client->name, 0, NAME_LEN);
      strncat(client->name, name, NAME_LEN);
      strncat(data->name, name, NAME_LEN);
      data->size_name = strlen(data->name);
    }
  else
    return (cmd_error(srv, client, make_response(data, ERR_NICK,
						 ERRNICKUSED)));
  send_all_chans(client, &pair);
  return (cmd_success(srv, client, make_response(data, CMD_NICK, "Success")));
}

int	send_private_msg(t_srv *srv, t_client *client, t_data *data)
{
  char		*name;
  char		*msg;
  t_client	*dest;

  (void) strtok(data->content, " \t");
  if (!(name = strtok(NULL, " \t")) || !(msg = strtok(NULL, "\r")))
    return (cmd_error(srv, client, make_response(data, ERR_PMSG,
						 ERRNOPARAM)));
  if (!(dest = srv->users_list.get(&srv->users_list, (t_pred) &cmp_nickname,
				   name)))
    return (cmd_error(srv, client, make_response(data, ERR_PMSG,
						 ERRNOUSER)));
  memset(data->content, 0, CONTENT_LEN);
  data->code = CMD_MSG;
  data->size = snprintf(data->content, CONTENT_LEN, "{%s} %s",
			client->name, msg);
  dest->buffer.write(&dest->buffer, data);
  FD_SET(dest->fd, &srv->writefds);
  return (TRUE);
}

int	send_msg(t_srv *srv, t_client *client, t_data *data)
{
  t_chan	*chan;
  t_pair_data	pair;

  if (!(chan = srv->chans_list.get(&srv->chans_list, (t_pred) &cmp_channame,
				   data->name)))
    return (cmd_error(srv, client, make_response(data, ERR_MSG,
						 "Chan does not exist")));
  make_pair_response(srv, &pair);
  pair.data.size = snprintf(pair.data.content, CONTENT_LEN, "{%s}<%s> %s",
			    data->name, client->name, data->content);
  strncat(pair.data.name, data->name, NAME_LEN);
  pair.data.size_name = strlen(pair.data.name);
  send_all_users2(chan, &pair, client);
  return (TRUE);
}

int		join_channel(t_srv *srv, t_client *client, t_data *data)
{
  char		*chan_name;
  t_chan	*chan;
  t_pair_data	pair;

  (void) strtok(data->content, " \t");
  chan_name = strtok(NULL, " \t");
  if (!chan_name)
    return (cmd_error(srv, client, make_response(data, ERR_JOIN, ERRNOPARAM)));
  if (client->chans_list.get(&client->chans_list, (t_pred) &cmp_channame,
			     chan_name))
    return (cmd_error(srv, client, make_response(data, ERR_JOIN,
						 ERRCHANAJOIN)));
  if (!(chan = srv->chans_list.get(&srv->chans_list, (t_pred) &cmp_channame,
				   chan_name)))
    chan = create_channel(srv, chan_name);
  chan->users_list.add(&chan->users_list, client);
  client->chans_list.add(&client->chans_list, chan);
  make_pair_response(srv, &pair);
  pair.data.size = snprintf(pair.data.content, CONTENT_LEN, "%s enters %s",
			    client->name, chan_name);
  memset(data->name, 0, NAME_LEN);
  strncat(data->name, chan_name, NAME_LEN);
  data->size_name = strlen(data->name);
  send_all_users(chan, &pair);
  return (cmd_success(srv, client, make_response(data, CMD_JOIN, "Success")));
}

int	leave_channel(t_srv *srv, t_client *client, t_data *data)
{
  char		*chan_name;
  t_chan	*chan;
  t_pair_data	pair;

  (void) strtok(data->content, " \t");
  chan_name = strtok(NULL, " \t");
  if (!chan_name)
    return (cmd_error(srv, client, make_response(data, ERR_LEAVE,
						 ERRNOPARAM)));
  if (!(chan = client->chans_list.get(&client->chans_list,
				      (t_pred) &cmp_channame, chan_name)))
    return (cmd_error(srv, client, make_response(data, ERR_LEAVE, ERRNOCHAN)));
  chan->users_list.remove(&chan->users_list, client);
  client->chans_list.remove(&client->chans_list, chan);
  make_pair_response(srv, &pair);
  snprintf(pair.data.content, CONTENT_LEN, "%s leaves %s", client->name,
	   chan_name);
  memset(data->name, 0, NAME_LEN);
  strncat(data->name, chan_name, NAME_LEN);
  data->size_name = strlen(data->name);
  send_all_users(chan, &pair);
  return (cmd_success(srv, client, make_response(data, CMD_LEAVE, "Success")));
}
