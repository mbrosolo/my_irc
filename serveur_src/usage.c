/*
** usage.c for usage serveur in /home/brosol_m/docs/systeme_unix/myftp-2015-2014s-brosol_m/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Apr  6 19:43:23 2012 matthias brosolo
** Last update Fri Apr  6 19:43:23 2012 matthias brosolo
*/

#include	"serveur.h"

int	usage(char *name)
{
  fprintf(stderr, "[USAGE]: %s port\n", name);
  return (EXIT_FAILURE);
}
