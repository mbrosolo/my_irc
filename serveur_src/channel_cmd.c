/*
** channel_cmd.c for channel_cmd in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 23:16:22 2012 matthias brosolo
** Last update Thu Apr 19 23:16:22 2012 matthias brosolo
*/

#include	"serveur.h"

enum e_bool	send_user(t_client *client, t_client *c)
{
  t_data	data;

  memset(&data, 0, sizeof(data));
  data.code = CMD_MSG;
  data.magic = MAGIC;
  strncat(data.content, client->name, CONTENT_LEN);
  data.size = strlen(data.content);
  c->buffer.write(&c->buffer, &data);
  return (FALSE);
}

enum e_bool	send_list(t_chan *chan, t_client *c)
{
  t_data	data;

  memset(&data, 0, sizeof(data));
  data.code = CMD_MSG;
  data.magic = MAGIC;
  strncat(data.content, chan->name, CONTENT_LEN);
  data.size = strlen(data.content);
  c->buffer.write(&c->buffer, &data);
  return (FALSE);
}

enum e_bool	send_list_pattern(t_chan *chan, t_pair_pattern *p)
{
  t_data	data;

  if (!strstr(chan->name, p->pattern))
    return (FALSE);
  memset(&data, 0, sizeof(data));
  data.code = CMD_MSG;
  data.magic = MAGIC;
  strncat(data.content, chan->name, CONTENT_LEN);
  data.size = strlen(data.content);
  p->client->buffer.write(&p->client->buffer, &data);
  return (FALSE);
}

int		list_users(t_srv *srv, t_client *client, t_data *data)
{
  t_chan	*chan;

  if (!(chan = srv->chans_list.get(&srv->chans_list, (t_pred) &cmp_channame,
				   data->name)))
    return (FALSE);
  memset(data->content, 0, CONTENT_LEN);
  data->code = CMD_MSG;
  strncat(data->content, "Begin of /users ->", CONTENT_LEN);
  data->size = strlen(data->content);
  client->buffer.write(&client->buffer, data);
  chan->users_list.get(&chan->users_list, (t_pred) &send_user, client);
  strncpy(data->content, "<- End of /users", CONTENT_LEN);
  data->size = strlen(data->content);
  client->buffer.write(&client->buffer, data);
  FD_SET(client->fd, &srv->writefds);
  return (TRUE);
}

int			list_channels(t_srv *srv, t_client *client, t_data *data)
{
  char			*pattern;
  t_pair_pattern	p;

  (void) strtok(data->content, " \t");
  if ((pattern = strtok(NULL, " \t")))
    pattern = strdup(pattern);
  memset(data->content, 0, CONTENT_LEN);
  data->code = CMD_MSG;
  strncat(data->content, "Begin of /list ->", CONTENT_LEN);
  data->size = strlen(data->content);
  client->buffer.write(&client->buffer, data);
  if (!pattern)
    srv->chans_list.get(&srv->chans_list, (t_pred) &send_list, client);
  else
    {
      p.pattern = pattern;
      p.client = client;
      srv->chans_list.get(&srv->chans_list, (t_pred) &send_list_pattern, &p);
      free(pattern);
    }
  strncpy(data->content, "<- End of /list", CONTENT_LEN);
  data->size = strlen(data->content);
  client->buffer.write(&client->buffer, data);
  FD_SET(client->fd, &srv->writefds);
  return (TRUE);
}
