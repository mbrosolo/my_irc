/*
** init_srv.c for init_srv in /home/brosol_m/docs/systeme_unix/my_irc/my_irc_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 16:55:12 2012 matthias brosolo
** Last update Thu Apr 19 16:55:12 2012 matthias brosolo
*/

#include	"serveur.h"

void		init_cmd_array(t_cmd *array)
{
  strcpy(array[CMD_NICK].cmd, "/nick");
  array[CMD_NICK].fct = &change_nickname;
  strcpy(array[CMD_LIST].cmd, "/list");
  array[CMD_LIST].fct = &list_channels;
  strcpy(array[CMD_JOIN].cmd, "/join");
  array[CMD_JOIN].fct = &join_channel;
  strcpy(array[CMD_LEAVE].cmd, "/part");
  array[CMD_LEAVE].fct = &leave_channel;
  strcpy(array[CMD_USERS].cmd, "/users");
  array[CMD_USERS].fct = &list_users;
  array[CMD_MSG].fct = &send_msg;
  strcpy(array[CMD_PMSG].cmd, "/msg");
  array[CMD_PMSG].fct = &send_private_msg;
  strcpy(array[CMD_SEND].cmd, "/send_file");
  array[CMD_SEND].fct = &send_file;
  strcpy(array[CMD_ACCEPT].cmd, "/accept_file");
  array[CMD_ACCEPT].fct = &accept_file;
}

void	init_srv(t_srv *srv, int sockfd)
{
  FD_ZERO(&srv->writefds);
  init_list(&srv->users_list);
  init_list(&srv->chans_list);
  srv->fdmax = sockfd;
  srv->sockfd = sockfd;
  memset(srv->cmd, 0, sizeof(srv->cmd));
  init_cmd_array(srv->cmd);
}
