/*
** send_all.c for send_all in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Apr 22 20:38:54 2012 matthias brosolo
** Last update Sun Apr 22 23:08:23 2012 anna texier
*/

#include	"serveur.h"

enum e_bool	set_client_buffer(t_client *client, t_pair_data *p)
{
  client->buffer.write(&client->buffer, &p->data);
  FD_SET(client->fd, &p->srv->writefds);
  return (FALSE);
}

void	send_all_users2(t_chan *chan, t_pair_data *p, t_client *c)
{
  t_list	*tmp;

  tmp = chan->users_list.begin;
  while (tmp)
    {
      if (((t_client *) tmp->data) != c)
	{
	  ((t_client *) tmp->data)->buffer.write(&(((t_client *) tmp->data)
						   ->buffer), &p->data);
	  FD_SET(((t_client *) tmp->data)->fd, &p->srv->writefds);
	}
      tmp = tmp->next;
    }
}

enum e_bool	send_all_users(t_chan *chan, t_pair_data *p)
{
  chan->users_list.get(&chan->users_list, (t_pred) &set_client_buffer, p);
  return (FALSE);
}

void	send_all_chans(t_client *client, t_pair_data *p)
{
  client->chans_list.get(&client->chans_list, (t_pred) &send_all_users, p);
}
