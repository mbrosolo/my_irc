/*
** cmd_error.c for cmd_error in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Apr 20 11:33:21 2012 matthias brosolo
** Last update Sun Apr 22 18:40:34 2012 anna texier
*/

#include	"serveur.h"

void	make_pair_response(t_srv *srv, t_pair_data *p)
{
  p->srv = srv;
  memset(&p->data, 0, sizeof(p->data));
  p->data.code = CMD_MSG;
  p->data.magic = MAGIC;
}

int	cmd_error(t_srv *srv, t_client *client, t_data *data)
{
  client->buffer.write(&client->buffer, data);
  FD_SET(client->fd, &srv->writefds);
  return (FALSE);
}

t_data	*make_response(t_data *data, enum e_type code, char *content)
{
  data->code = code;
  data->magic = MAGIC;
  memset(data->content, 0, CONTENT_LEN);
  strncat(data->content, content, CONTENT_LEN);
  data->size = strlen(data->content);
  return (data);
}

int	cmd_success(t_srv *srv, t_client *client, t_data *data)
{
  client->buffer.write(&client->buffer, data);
  FD_SET(client->fd, &srv->writefds);
  return (TRUE);
}
