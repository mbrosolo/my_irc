/*
** process_client.c for process_client in /home/brosol_m/docs/systeme_unix/my_irc/my_irc_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 09:26:25 2012 matthias brosolo
** Last update Thu Apr 19 09:26:25 2012 matthias brosolo
*/

#include	"serveur.h"

void		set_client_fd(t_srv *srv)
{
  t_list	*tmp;
  t_client	*data;

  FD_ZERO(&srv->readfds);
  tmp = srv->users_list.begin;
  while (tmp)
    {
      data = (t_client *) tmp->data;
      FD_SET(data->fd, &srv->readfds);
      srv->fdmax = MAX(srv->fdmax, data->fd);
      tmp = tmp->next;
    }
}

void		add_client(t_srv *srv)
{
  t_client	client;
  t_client	*tmp;

  memset(&client, 0, sizeof(client));
  client.sock_len = sizeof(client.sock_addr);
  if ((client.fd = accept(srv->sockfd, (struct sockaddr *) &client.sock_addr,
			  &client.sock_len)) == -1
      || client.sock_len != sizeof(client.sock_addr))
    return ;
  tmp = xmalloc(sizeof(*tmp));
  memcpy(tmp, &client, sizeof(client));
  init_cbuffer(&tmp->buffer);
  init_list(&tmp->users_list);
  init_list(&tmp->chans_list);
  tmp->flag = FALSE;
  srv->users_list.add(&srv->users_list, tmp);
}

enum e_bool	read_client(t_client *client, t_srv *srv)
{
  if (FD_ISSET(client->fd, &srv->readfds))
    process_read_client(srv, client);
  return (FALSE);
}

enum e_bool	write_client(t_client *client, t_srv *srv)
{
  if (FD_ISSET(client->fd, &srv->writefds))
    process_write_client(srv, client);
  return (FALSE);
}

/* enum e_bool	printclient(t_client *client, void *toto) */
/* { */
/*   printf("NAME CLIENT %s\n", client->name); */
/*   return (FALSE); */
/* } */
/* enum e_bool	printchan(t_chan *chan, void *toto) */
/* { */
/*   printf("NAME CHAN %s\n", chan->name); */
/*   chan->users_list.get(&chan->users_list, (t_pred) &printclient, */
/* 		      NULL);   */
/*   return (FALSE); */
/* } */
/* printf("%p\n", srv.chans_list.begin); */
/* srv.chans_list.get(&srv.chans_list, (t_pred) &printchan, */
/* 		     NULL); */
void	process_server(int sockfd)
{
  int	rs;
  t_srv	srv;

  init_srv(&srv, sockfd);
  while (TRUE)
    {
      set_client_fd(&srv);
      FD_SET(sockfd, &srv.readfds);
      if ((rs = select(srv.fdmax + 1, &srv.readfds, &srv.writefds, NULL, NULL))
	  == -1)
	socket_die("select()", sockfd, NULL);
      else if (rs)
	{
	  if (FD_ISSET(sockfd, &srv.readfds))
	    add_client(&srv);
	  else
	    {
	      srv.users_list.get(&srv.users_list, (t_pred) write_client, &srv);
	      srv.users_list.get(&srv.users_list, (t_pred) read_client, &srv);
	    }
	}
    }
}
