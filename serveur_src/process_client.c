/*
** process_client.c for process_client.c in /home/brosol_m/docs/systeme_unix/my_irc/my_irc_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 17:41:02 2012 matthias brosolo
** Last update Sun Apr 22 18:10:54 2012 anna texier
*/

#include	"serveur.h"

void	send_all(t_srv *srv, t_client *data, t_data *buffer)
{
  t_list	*tmp;
  t_client	*data_tmp;

  tmp = srv->users_list.begin;
  while (tmp)
    {
      data_tmp = (t_client *) tmp->data;
      if (data_tmp->fd != data->fd
	  && data_tmp->buffer.write(&data_tmp->buffer, buffer) == TRUE)
	FD_SET(data_tmp->fd, &srv->writefds);
      tmp = tmp->next;
    }
}

void		process_read_client(t_srv *srv, t_client *client)
{
  int		r;
  t_data	buffer;

  if ((r = read(client->fd, &buffer, sizeof(buffer))) == -1 || !r)
    remove_client(srv, client);
  else if (buffer.magic == MAGIC && buffer.size <= CONTENT_LEN
	   && !buffer.content[buffer.size] && buffer.size_name <= NAME_LEN
	   && !buffer.name[buffer.size_name])
    {
      if (buffer.content[0] != '/')
	srv->cmd[CMD_MSG].fct(srv, client, &buffer);
      else
	{
	  r = CMD_NICK - 1;
	  while (++r <= CMD_ACCEPT)
	    if (r != CMD_MSG && !strncmp(srv->cmd[r].cmd, buffer.content,
					 strlen(srv->cmd[r].cmd)))
	      {
		srv->cmd[r].fct(srv, client, &buffer);
		return;
	      }
	}
    }
}

void	process_write_client(t_srv *srv, t_client *client)
{
  t_data	*buffer;

  if (!(buffer = client->buffer.read(&client->buffer)))
    return;
  if (write(client->fd, buffer, sizeof(*buffer)) == -1)
    remove_client(srv, client);
  if (!client->buffer.empty(&client->buffer))
    FD_CLR(client->fd, &srv->writefds);
}
