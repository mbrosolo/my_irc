/*
** file_cmd.c for file_cmd in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 23:18:10 2012 matthias brosolo
** Last update Thu Apr 19 23:18:10 2012 matthias brosolo
*/

#include	"serveur.h"

int	send_file(t_srv *srv, t_client *client, t_data *data)
{
  (void) srv;
  (void) client;
  (void) data;
  return (TRUE);
}

int	accept_file(t_srv *srv, t_client *client, t_data *data)
{
  (void) srv;
  (void) client;
  (void) data;
  return (TRUE);
}
