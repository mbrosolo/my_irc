/*
** channels.c for channels in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Apr 20 18:48:19 2012 matthias brosolo
** Last update Fri Apr 20 18:48:19 2012 matthias brosolo
*/

#include	"serveur.h"

t_chan		*create_channel(t_srv *srv, char *name)
{
  t_chan	*chan;

  chan = xmalloc(sizeof(*chan));
  memset(chan->name, 0, CHAN_NAME_LEN);
  strncat(chan->name, name, CHAN_NAME_LEN);
  init_list(&chan->users_list);
  srv->chans_list.add(&srv->chans_list, chan);
  return (chan);
}
