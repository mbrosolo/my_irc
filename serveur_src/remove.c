/*
** remove.c for remove in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Apr 22 20:10:33 2012 matthias brosolo
** Last update Sun Apr 22 20:10:33 2012 matthias brosolo
*/

#include	"serveur.h"

enum e_bool	remove_from_chan(t_chan *chan, t_client *client)
{
  chan->users_list.remove(&chan->users_list, client);
  return (FALSE);
}

enum e_bool	remove_from_client(t_client *client_from, t_client *client)
{
  client_from->users_list.remove(&client_from->users_list, client);
  return (FALSE);
}

void	remove_client(t_srv *srv, t_client *client)
{
  printf("[%s] disconnected\n", client->name);
  FD_CLR(client->fd, &srv->readfds);
  FD_CLR(client->fd, &srv->writefds);
  close(client->fd);
  srv->chans_list.get(&srv->chans_list, (t_pred) remove_from_chan, client);
  srv->users_list.get(&srv->users_list, (t_pred) remove_from_client, client);
  srv->users_list.remove(&srv->users_list, client);
  free(client);
}
