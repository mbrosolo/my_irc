/*
** serveur.h for serveur in /home/brosol_m/docs/systeme_unix/my_ftp/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Mar 28 15:24:27 2012 matthias brosolo
** Last update Sun Apr 22 23:10:39 2012 anna texier
*/

#ifndef	__SERVEUR_H__
# define	__SERVEUR_H__

# include	"my_irc.h"

# define	ERRNOPARAM	("Not enough parameters given.")
# define	ERRNICKUSED	("Nickname already used.")
# define	ERRNOCHAN	("Not on the channel.")
# define	ERRNOUSER	("No such user.")
# define	ERRCHANAJOIN	("You've already joined this channel.")

# define	MAX(x, y)	((x) > (y) ? (x) : (y))
# define	MAX_CONNECTION	(128)

/*
** STRUCTURES
*/
typedef	struct	s_chan
{
  char		name[CHAN_NAME_LEN + 1];
  t_container	users_list;
}		t_chan;

typedef	struct	s_client
{
  int		fd;
  char		name[NAME_LEN + 1];
  char		current[CONTENT_LEN + 1];
  t_cbuffer	buffer;
  socklen_t	sock_len;
  t_container	users_list;
  t_container	chans_list;
  enum e_bool	flag;
  struct sockaddr_in	sock_addr;
}		t_client;

typedef struct s_srv	t_srv;

typedef	struct	s_cmd
{
  char	cmd[CMD_LEN + 1];
  int	(*fct)(struct s_srv *srv, t_client *client, t_data *data);
}		t_cmd;

struct	s_srv
{
  int		fdmax;
  int		sockfd;
  fd_set	readfds;
  fd_set	writefds;
  t_cmd		cmd[CMD_ACCEPT + 1];
  t_container	chans_list;
  t_container	users_list;
};

typedef	struct	s_pair_data
{
  t_srv		*srv;
  t_data	data;
}		t_pair_data;

typedef	struct	s_pair_pattern
{
  t_client	*client;
  char		*pattern;
}		t_pair_pattern;

/*
** cmd_response.c
*/
int	cmd_error(t_srv *srv, t_client *client, t_data *data);
int	cmd_success(t_srv *srv, t_client *client, t_data *data);
t_data	*make_response(t_data *data, enum e_type code, char *success);
void	make_pair_response(t_srv *srv, t_pair_data *p);
/*
** channels.c
*/
t_chan		*create_channel(t_srv *srv, char *name);

/*
** channel_cmd.c
*/
int	list_users(t_srv *srv, t_client *client, t_data *data);
int	list_channels(t_srv *srv, t_client *client, t_data *data);

/*
** file_cmd.c
*/
int	send_file(t_srv *srv, t_client *client, t_data *data);
int	accept_file(t_srv *srv, t_client *client, t_data *data);

/*
** create_socket.c
*/
int	create_socket(char *port);

/*
** init_srv.c
*/
void	init_srv(t_srv *srv, int sockfd);

/*
** process_client.c
*/
void	process_read_client(t_srv *srv, t_client *data);
void	process_write_client(t_srv *srv, t_client *data);

/*
** predicate.c
*/
enum e_bool	cmp_nickname(t_client *client, char *s);
enum e_bool	cmp_channame(t_chan *chan, char *s);

/*
** process_server.c
*/
void	process_server(int sockfd);

/*
** remove.c
*/
void	remove_client(t_srv *srv, t_client *client);

/*
** send_all.c
*/
void	send_all_chans(t_client *client, t_pair_data *p);
enum e_bool	send_all_users(t_chan *chan, t_pair_data *p);
enum e_bool	set_client_buffer(t_client *client, t_pair_data *p);

/*
** user_cmd.c
*/
void	send_all_users2(t_chan *chan, t_pair_data *p, t_client *c);
int	join_channel(t_srv *srv, t_client *client, t_data *data);
int	leave_channel(t_srv *srv, t_client *client, t_data *data);
int	change_nickname(t_srv *srv, t_client *client, t_data *data);
int	send_msg(t_srv *srv, t_client *client, t_data *data);
int	send_private_msg(t_srv *srv, t_client *client, t_data *data);

#endif
