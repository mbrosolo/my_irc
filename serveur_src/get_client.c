/*
** get_client.c for get_client in /home/brosol_m/docs/systeme_unix/myftp-2015-2014s-brosol_m/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Fri Apr  6 21:55:43 2012 matthias brosolo
** Last update Fri Apr  6 21:55:43 2012 matthias brosolo
*/

#include	"serveur.h"

void	start_client(int sockfd_client)
{
  int	r;
  char	mark;
  char	buffer[256];

  mark = 1;
  while (mark)
    {
      if ((r = read(sockfd_client, buffer, sizeof(buffer) - 1)) == -1)
	socket_die("read()", sockfd_client, NULL);
      if (r > 0 && buffer[r - 1] == '\n')
	buffer[r - 1] = '\0';
      buffer[r] = '\0';
      if (!exec_command(sockfd_client, buffer, &mark, r))
	xwrite(sockfd_client, "ERROR: Invalid command\n",
	       sizeof("ERROR: Invalid command\n"));
    }
  if (close(sockfd_client) == -1)
    ret_error("close()", NULL);
}

int	get_new_client(int sockfd)
{
  int			sockfd_client;
  pid_t			pid;
  socklen_t		socklen;
  struct sockaddr_in	sock_addr_client;

  socklen = sizeof(sock_addr_client);
  if ((sockfd_client = accept(sockfd, (struct sockaddr *) &sock_addr_client,
			      &socklen)) == -1
      || socklen != sizeof(sock_addr_client))
    {
      close(sockfd);
      return (ret_error("accept()", NULL));
    }
  if ((pid = fork()) == -1)
    return (ret_error("fork()", NULL));
  else if (!pid)
    {
      close(sockfd);
      start_client(sockfd_client);
      exit(0);
    }
  else
    close(sockfd_client);
  return (sockfd_client);
}
