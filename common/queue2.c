/*
** queue2.c for queue2 in /home/brosol_m/docs/systeme_unix/my_irc/my_irc_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 20:20:43 2012 matthias brosolo
** Last update Thu Apr 19 20:20:43 2012 matthias brosolo
*/

#include	"list.h"

static int	size(t_queue *q)
{
  return (q->_size);
}

void	init_queue_second(t_queue *q)
{
  q->size = &size;
}
