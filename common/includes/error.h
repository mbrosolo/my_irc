/*
** error.h for error.h in /home/brosol_m/docs/systeme_unix/my_ftp/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Mar 29 22:32:45 2012 matthias brosolo
** Last update Thu Mar 29 22:32:45 2012 matthias brosolo
*/

#ifndef	__ERROR_H__
# define	__ERROR_H__

# include	<errno.h>
# include	<stdio.h>
# include	<stdlib.h>
# include	<string.h>
# include	<unistd.h>

/*
** error.c
*/
int	die(char *fct, char *error);
int	socket_die(char *fct, int socket, char *error);
int	ret_error(char *fct, char *error);
int	usage(char *name);

#endif
