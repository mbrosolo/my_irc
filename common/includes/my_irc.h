/*
** my_irc.h for my_irc in /home/brosol_m/docs/systeme_unix/myftp-2015-2014s-brosol_m/my_irc_client
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr  5 14:13:31 2012 matthias brosolo
** Last update Sun Apr 22 17:45:53 2012 anna texier
*/

#ifndef	__MY_IRC_H__
#define	__MY_IRC_H__

/*
** INCLUDES
*/
#include	<arpa/inet.h>
#include	<netdb.h>
#include	<netinet/in.h>
#include	<netinet/ip.h>
#include	<signal.h>
#include	<sys/types.h>
#include	<sys/select.h>
#include	<sys/socket.h>
#include	"list.h"

/*
** DEFINES
*/
#define	TCP		("TCP")
#define	MAGIC		(0x49B8DE7A6CAD46C7)

#define	CMD_LEN		(20)

#define	NAME_LEN	(20)
#define	CHAN_NAME_LEN	(20)
#define	CONTENT_LEN	(1024)
#define	FILENAME_LEN	(255)

#define	DEFAULT_PORT	(2172)

typedef	enum e_bool	(*t_pred)(void *, void *);

/*
** ENUM
*/
enum	e_type
  {
    ERROR = -1,
    CMD_NICK = 0,
    CMD_LIST,
    CMD_JOIN,
    CMD_LEAVE,
    CMD_USERS,
    CMD_MSG,
    CMD_PMSG,
    CMD_SEND,
    CMD_ACCEPT,
    ERR_NICK,
    ERR_LIST,
    ERR_JOIN,
    ERR_LEAVE,
    ERR_USERS,
    ERR_MSG,
    ERR_PMSG,
    ERR_SEND,
    ERR_ACCEPT,
    CMD_NUM
  };

/*
** STRUCTURES
*/
typedef	struct	s_fd_pair
{
  fd_set	*fds;
  int		fd;
}		t_fd_pair;

typedef	struct	s_data
{
  enum e_type	code;
  size_t	magic;
  size_t	size;
  size_t	size_name;
  char		content[CONTENT_LEN + 1];
  char		name[CHAN_NAME_LEN + 1];
}		t_data;

typedef	struct	s_cbuffer
{
  int		(*empty)(struct s_cbuffer *b);
  t_data	*(*read)(struct s_cbuffer *b);
  int		(*write)(struct s_cbuffer *b, t_data const *data);
  t_queue	queue;
}		t_cbuffer;

/*
** PROTOTYPES
*/
/*
** cbuffer.c
*/
void	init_cbuffer(t_cbuffer *b);

#endif
