/*
** xfunction.h for xfunctions in /home/brosol_m/docs/systeme_unix/myftp-2015-2014s-brosol_m/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Apr  8 17:51:25 2012 matthias brosolo
** Last update Sun Apr  8 17:51:25 2012 matthias brosolo
*/

#ifndef	__XFUNCTIONS_H__
# define	__XFUNCTIONS_H__

# include	"error.h"

/*
** xfunctions.c
*/
void	*xmalloc(size_t size);
ssize_t	xwrite(int fd, const void *buf, size_t count);

#endif
