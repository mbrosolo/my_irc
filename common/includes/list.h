/*
** list.h for list in /home/brosol_m/docs/systeme_unix/my_irc/common
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Apr 18 16:50:19 2012 matthias brosolo
** Last update Sun Apr 22 21:48:09 2012 anna texier
*/

#ifndef	__LIST_H__
# define	__LIST_H__

# include	<stdlib.h>
# include	"xfunctions.h"

enum	e_bool
  {
    FALSE = 0,
    TRUE
  };

typedef	struct	s_list
{
  void		*data;
  struct s_list	*prev;
  struct s_list	*next;
}		t_list;

typedef	struct	s_queue
{
  size_t	_size;
  t_list	*begin;
  t_list	*end;
  int		(*size)(struct s_queue *q);
  void		*(*front)(struct s_queue *q);
  void		(*push)(struct s_queue *q, void *data);
  void		(*pop)(struct s_queue *q);
  void		(*delete)(struct s_queue *q);
}		t_queue;

typedef	struct	s_container
{
  size_t	size;
  t_list	*begin;
  t_list	*end;
  void		(*add)(struct s_container *, void *);
  void		*(*get)(struct s_container *, enum e_bool (*pred)(void *, void *), void *);
  void		(*remove)(struct s_container *, void *);
  void		(*delete)(struct s_container *);
}		t_container;

/*
** PROTOTYES
*/
/*
** list.c
*/
void	init_list(t_container *cont);

/*
** list_remove.c
*/
void	remove_data(t_container *cont, void *data);

/*
** queue.c
*/
void	init_queue(t_queue *q);
void	init_queue_second(t_queue *q);
#endif
