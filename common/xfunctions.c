/*
** xfunctions.c for xfunctions in /home/brosol_m/docs/systeme_unix/myftp-2015-2014s-brosol_m/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Apr  8 16:02:01 2012 matthias brosolo
** Last update Sun Apr  8 16:02:01 2012 matthias brosolo
*/

#include	"my_irc.h"

void	*xmalloc(size_t size)
{
  void	*ptr;

  if (!(ptr = malloc(size)))
    die("malloc()", NULL);
  return (ptr);
}

ssize_t	xwrite(int fd, const void *buf, size_t count)
{
  ssize_t	r;

  if ((r = write(fd, buf, count)) != (ssize_t) count)
    return (ret_error("write()", "All bytes did not written"));
  return (r);
}
