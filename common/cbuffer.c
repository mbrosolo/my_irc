/*
** cbuffer.c for cbuffer in /home/brosol_m/docs/systeme_unix/my_irc/my_irc_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 18:51:17 2012 matthias brosolo
** Last update Thu Apr 19 18:51:17 2012 matthias brosolo
*/

#include	"my_irc.h"

static t_data	*_read(t_cbuffer *b)
{
  t_data	*data;

  data = b->queue.front(&b->queue);
  b->queue.pop(&b->queue);
  return (data);
}

static int	_write(t_cbuffer *b, t_data const *data)
{
  t_data	*tmp;

  if (!(tmp = xmalloc(sizeof(*tmp))))
    return (FALSE);
  memcpy(tmp, data, sizeof(*tmp));
  b->queue.push(&b->queue, tmp);
  return (TRUE);
}

static int	empty(t_cbuffer *b)
{
  return (b->queue.size(&b->queue));
}

void	init_cbuffer(t_cbuffer *b)
{
  init_queue(&b->queue);
  b->read = &_read;
  b->write = &_write;
  b->empty = &empty;
}
