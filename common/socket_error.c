/*
** client_error.c for client_error in /home/brosol_m/docs/systeme_unix/myftp-2015-2014s-brosol_m/my_ftp_serveur
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Apr  8 16:12:10 2012 matthias brosolo
** Last update Sun Apr  8 16:12:10 2012 matthias brosolo
*/

#include	"my_ftp.h"

int	socket_error(int sockfd_client, char *fct)
{
  int	err;
  char	*error;

  err = errno;
  xwrite(sockfd_client, fct, strlen(fct));
  xwrite(sockfd_client, ": ", sizeof(": "));
  if (!(error = strerror(err)))
    xwrite(sockfd_client, "Unknown error", sizeof("Unknown error"));
  else
    xwrite(sockfd_client, error, strlen(error));
  xwrite(sockfd_client, "\n", sizeof("\n"));
  return (TRUE);
}
