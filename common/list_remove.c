/*
** list_remove.c for list_remove in /home/brosol_m/docs/systeme_unix/my_irc/serveur_src
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Sun Apr 22 20:57:15 2012 matthias brosolo
** Last update Sun Apr 22 20:57:15 2012 matthias brosolo
*/

#include	"list.h"

static void	assign_ptr(t_list **first, t_list *value,
			   t_list **second, char flag)
{
  *first = value;
  if (*first)
    {
      if (!flag)
	(*first)->prev = NULL;
      else
	(*first)->next = NULL;
    }
  else
    *second = NULL;
}

void		remove_data(t_container *cont, void *data)
{
  t_list	*tmp;

  tmp = cont->begin;
  while (tmp && tmp->data != data)
    tmp = tmp->next;
  if (!tmp)
    return;
  if (tmp == cont->begin)
    assign_ptr(&cont->begin, cont->begin->next, &cont->end, 0);
  else if (tmp == cont->end)
    assign_ptr(&cont->end, cont->end->prev, &cont->begin, 1);
  else
    {
      tmp->prev->next = tmp->next;
      tmp->next->prev = tmp->prev;
    }
  free(tmp);
  --cont->size;
}
