/*
** error.c for error in /home/brosol_m/docs/systeme_unix/nm-objdump
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Mar  7 09:51:20 2012 matthias brosolo
** Last update Thu Mar  8 10:49:06 2012 matthias brosolo
*/

#include	"error.h"

int	die(char *fct, char *error)
{
  int	err;

  err = errno;
  fprintf(stderr, "[%s]: %s\n", fct, (error ? (error) : strerror(err)));
  exit(EXIT_FAILURE);
}

int	socket_die(char *fct, int socket, char *error)
{
  int	err;

  err = errno;
  close(socket);
  fprintf(stderr, "[%s]: %s\n", fct, (error ? (error) : strerror(err)));
  exit(EXIT_FAILURE);
}

int	ret_error(char *fct, char *error)
{
  int	err;

  err = errno;
  fprintf(stderr, "[%s]: %s\n", fct, (error ? (error) : strerror(err)));
  return (EXIT_FAILURE);
}
