/*
** list.c for list in /home/brosol_m/docs/systeme_unix/my_irc/common
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Wed Apr 18 16:48:35 2012 matthias brosolo
** Last update Sun Apr 22 22:57:52 2012 anna texier
*/

#include	"list.h"

static void		add(t_container *cont, void *data)
{
  t_list	*new;

  new = xmalloc(sizeof(*new));
  new->data = data;
  new->prev = cont->end;
  new->next = NULL;
  if (cont->begin == NULL)
    {
      cont->begin = new;
      cont->end = new;
    }
  else
    {
      cont->end->next = new;
      cont->end = new;
    }
  ++cont->size;
}

static void		*get(t_container *c,
			     enum e_bool (*pred)(void *, void *), void *data)
{
  t_list	*list;
  t_list	*back;

  list = c->begin;
  while (list != NULL)
    {
      back = list->next;
      if (pred(list->data, data) == TRUE)
	return (list->data);
      list = back;
    }
  return (NULL);
}

static void		delete(t_container *cont)
{
  t_list	*tmp;

  while (cont->begin)
    {
      tmp = cont->begin->next;
      free(cont->begin);
      cont->begin = tmp;
    }
  cont->begin = NULL;
  cont->end = NULL;
  cont->size = 0;
}

void		init_list(t_container *cont)
{
  cont->size = 0;
  cont->begin = NULL;
  cont->end = NULL;
  cont->add = &add;
  cont->get = &get;
  cont->remove = &remove_data;
  cont->delete = &delete;
}
