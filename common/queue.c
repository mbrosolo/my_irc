/*
** queue.c for queue in /home/brosol_m/docs/systeme_unix/my_irc/common
** 
** Made by matthias brosolo
** Login   <brosol_m@epitech.net>
** 
** Started on  Thu Apr 19 15:43:11 2012 matthias brosolo
** Last update Thu Apr 19 15:43:11 2012 matthias brosolo
*/

#include	"list.h"

static void		push(t_queue *q, void *data)
{
  t_list	*new;

  new = xmalloc(sizeof(*new));
  new->data = data;
  new->next = NULL;
  if (q->begin == NULL)
    {
      q->begin = new;
      q->end = new;
    }
  else
    {
      q->end->next = new;
      q->end = new;
    }
  ++q->_size;
}

static void		*front(t_queue *q)
{
  return (q->begin ? q->begin->data : NULL);
}

static void		pop(t_queue *q)
{
  t_list	*tmp;

  if (!q->begin)
    return;
  tmp = q->begin;
  if (!q->begin->next)
    q->end = NULL;
  q->begin = q->begin->next;
  --q->_size;
}

static void		delete(t_queue *q)
{
  t_list	*tmp;

  while (q->begin)
    {
      tmp = q->begin->next;
      free(q->begin->data);
      free(q->begin);
      q->begin = tmp;
    }
  q->begin = NULL;
  q->end = NULL;
  q->_size = 0;
}

void		init_queue(t_queue *q)
{
  q->_size = 0;
  q->begin = NULL;
  q->end = NULL;
  q->push = &push;
  q->front = &front;
  q->pop = &pop;
  q->delete = &delete;
  init_queue_second(q);
}
