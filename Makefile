##
## Makefile for make my_irc in /home/brosol_m/docs/systeme_unix/my_irc
## 
## Made by matthias brosolo
## Login   <brosol_m@epitech.net>
## 
## Started on  Mon Apr 16 17:50:04 2012 matthias brosolo
## Last update Thu Apr 19 19:48:15 2012 anna texier
##

all:
	make -C ./serveur_src
	make -C ./client_src

clean:
	make clean -C ./serveur_src
	make clean -C ./client_src

fclean:
	make fclean -C ./serveur_src
	make fclean -C ./client_src

re:
	make re -C ./serveur_src
	make re -C ./client_src

.PHONY:	all clean fclean re