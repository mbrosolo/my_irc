/*
** list_infos.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sat Apr 21 11:25:48 2012 anna texier
** Last update Sun Apr 22 20:06:59 2012 anna texier
*/

#include	"client.h"

void	list_channels(t_data *data, t_client *infos)
{
  if (data->magic != MAGIC || data->size > CONTENT_LEN ||
      strlen(data->content) != data->size)
    return;
  if (data->code == ERR_LIST)
    {
      add_to_chat(data->content, ERROR, infos);
      return;
    }
  add_to_chat(data->name, CMD_NUM, infos);
}

void	list_users(t_data *data, t_client *infos)
{
  if (data->magic != MAGIC || data->size > CONTENT_LEN ||
      strlen(data->content) != data->size)
    return;
  if (data->code == ERR_USERS)
    {
      add_to_chat(data->content, ERROR, infos);
      return;
    }
  add_to_chat(data->content, CMD_NUM, infos);
}
