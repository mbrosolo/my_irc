/*
** send.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Thu Apr 19 17:21:37 2012 anna texier
** Last update Sun Apr 22 20:19:00 2012 anna texier
*/

#include	"client.h"

int		send_to_server(char *str, char *chan, t_client *infos)
{
  t_data	data;

  memset(&data, 0, sizeof(data));
  data.magic = MAGIC;
  data.size = strlen(str);
  data.size_name = strlen(chan);
  strncat(data.content, str, CONTENT_LEN);
  strncat(data.name, chan, NAME_LEN);
  if (write(infos->fd_socket, &data, sizeof(data)) < 0)
    die("write", NULL);
  return (0);
}

int		send_login(t_client *infos)
{
  t_data	data;
  char		*login;

  if ((login = getenv("USERNAME")) == NULL && (login = getenv("USER")) == NULL)
    login = DEFAULT_LOGIN;
  strncpy(infos->nickname, login, NAME_LEN);
  memset(&data, '\0', sizeof(data));
  data.code = CMD_NICK;
  data.magic = MAGIC;
  data.size = strlen(infos->nickname) + sizeof("/nick ");
  data.size_name = 0;
  strncat(data.content, "/nick ", CONTENT_LEN);
  strncat(data.content, infos->nickname, CONTENT_LEN);
  data.name[0] = '\0';
  if (write(infos->fd_socket, &data, sizeof(data)) < 0)
    die("write", NULL);
  return (0);
}
