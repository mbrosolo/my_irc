/*
** select.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Thu Apr 19 14:30:57 2012 anna texier
** Last update Sun Apr 22 22:25:08 2012 anna texier
*/

#include	"client.h"

static enum e_bool	set_fds(t_transfert *tmp, t_client *infos)
{
  FD_SET(tmp->read_fd, &(infos->readfds));
  if (tmp->read_fd > infos->max_fd)
    infos->max_fd = tmp->read_fd;
  return (FALSE);
}

static enum e_bool	check_fds(t_transfert *tmp, t_client *infos)
{
  if (FD_ISSET(tmp->read_fd, &(infos->readfds)))
    {
      if (tmp->read_fd == 0)
	read_client(infos);
      else if (tmp->read_fd == infos->fd_socket)
	read_server(infos);
    }
  return (FALSE);
}

int			exec_client(t_client *infos)
{
  struct timeval	timer;
  int			i;

  i = 0;
  add_fd(infos, 0, -1, "");
  while (52)
    {
      timer.tv_usec = 500000;
      timer.tv_sec = 0;
      FD_ZERO(&(infos->readfds));
      infos->max_fd = 0;
      infos->fd_list.get(&(infos->fd_list), (t_pred) &set_fds, infos);
      select((infos->max_fd) + 1, &(infos->readfds), NULL, NULL, NULL);
      infos->fd_list.get(&(infos->fd_list), (t_pred) &check_fds, infos);
    }
}
