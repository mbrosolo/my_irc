/*
** utils.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Tue Apr 17 15:35:03 2012 anna texier
** Last update Sun Apr 22 21:37:51 2012 anna texier
*/

#include	<ctype.h>
#include	"client.h"

int		pass_command(size_t i, char *str)
{
  while (i < CONTENT_LEN && !isspace(str[i]))
    i++;
  while (i < CONTENT_LEN && isspace(str[i]))
    i++;
  return (i);
}

int		spacecpy(size_t i, char *dest, char *src)
{
  size_t	j;

  j = 0;
  while (i < CONTENT_LEN && !isspace(src[i]))
    dest[j++] = src[i++];
  dest[j] = '\0';
  return (i);
}

void	clean()
{
  t_client	*infos;
  t_list	*tmp;

  free(get_array());
  infos = get_infos(NULL);
  tmp = infos->fd_list.begin;
  while (tmp)
    {
      if (((t_transfert *) tmp->data)->read_fd > 2)
	close(((t_transfert *) tmp->data)->read_fd);
      if (((t_transfert *) tmp->data)->write_fd > 2)
	close(((t_transfert *) tmp->data)->write_fd);
      free(tmp->data);
      tmp = tmp->next;
    }
  infos->fd_list.delete(&infos->fd_list);
  tmp = infos->channels.begin;
  while (tmp)
    {
      free(tmp->data);
      tmp = tmp->next;
    }
  infos->channels.delete(&infos->channels);
}
