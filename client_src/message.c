/*
** message.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sun Apr 22 16:47:36 2012 anna texier
** Last update Sun Apr 22 23:18:23 2012 anna texier
*/

#include	"client.h"

void	send_msg(t_data *d, t_client *infos)
{
  if (d->magic != MAGIC || d->size_name > NAME_LEN || d->size > CONTENT_LEN
      || d->name[d->size_name] != '\0' || d->content[d->size] != '\0')
    {
      printf("Invalid message\n");
      return;
    }
  if (d->code == ERR_MSG)
    {
      add_to_chat(d->content, ERROR, infos);
      return;
    }
  add_to_chat(d->content, CMD_NUM, infos);
}

void	send_private_msg(t_data *d, t_client *infos)
{
  if (d->magic != MAGIC || d->size_name > NAME_LEN || d->size > CONTENT_LEN
      || d->name[d->size_name] != '\0' || d->content[d->size] != '\0')
    return;
  if (d->code == ERR_PMSG)
    {
      add_to_chat(d->content, ERROR, infos);
      return;
    }
  add_to_chat(d->content, CMD_NUM, infos);
}
