/*
** client.h for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Tue Apr 17 14:05:16 2012 anna texier
** Last update Sun Apr 22 22:26:24 2012 anna texier
*/

#ifndef	__CLIENT_H_
#define	__CLIENT_H_

#include	<stdio.h>
#include	"my_irc.h"
#include	"list.h"

#define	DEFAULT_LOGIN	("username")

#define	CO_SUCCESS	("Connection success")
#define	CO_ERROR	("Couldn't connect to server\n")

#define	SOCKET_ERROR	("Can't create socket\n")

#define	PORT_ERROR	("Wrong port value\n")
#define	SPEC_ADDR	("Error, you have to specify an address\n")
#define	CONNECT_F	("You have to connect first to a server\n")

#define	READ_SOCK_ERR	("Connection closed by server\n")

#define	NICK_CHANGED	("You are now known as ")

typedef	struct	s_print
{
  enum e_type	type;
  size_t	size;
  char		content[CONTENT_LEN];
}		t_print;

typedef	struct	s_transfert
{
  int		read_fd;
  int		write_fd;
  char		file[FILENAME_LEN + 1];
}		t_transfert;

typedef	struct	s_client
{
  int		fd_socket;
  t_container	fd_list;
  fd_set	readfds;
  fd_set	writefds;
  int		max_fd;
  char		*current_chan;
  t_container	channels;
  char		nickname[NAME_LEN + 1];
}		t_client;

typedef struct	s_cmd
{
  char		cmd[CMD_LEN + 1];
  void		(*ptr)(t_data *, t_client *);
}		t_cmd;

/*
** accept_file.c
*/
void	accept_file(t_data *, t_client *);

/*
** adds.c
*/
void	add_fd(t_client *, int, int, char *);
int	add_to_chat(char *, enum e_type, t_client *);

/*
** chans.c
*/
void	change_nickname(t_data *, t_client *);
void	join_channel(t_data *, t_client *);
void	leave_channel(t_data *, t_client *);

/*
** connect.c
*/
int	connect_to_server(char *, t_client *);

/*
** list_infos.c
*/
void	list_channels(t_data *, t_client *);
void	list_users(t_data *, t_client *);

/*
** message.c
*/
void	send_msg(t_data *, t_client *);
void	send_private_msg(t_data *, t_client *);

/*
** ptr_func.c
*/
t_cmd	*get_array();
t_client	*get_infos(t_client *);

/*
** read.c
*/
int	read_client(t_client *);
int	read_server(t_client *);

/*
** select.c
*/
int	exec_client();

/*
** send.c
*/
int	send_to_server(char *, char *, t_client *);
int	send_login(t_client *);

/*
** send_file.c
*/
void	send_file(t_data *, t_client *);

/*
** utils.c
*/
int	pass_command(size_t, char *);
int	spacecpy(size_t, char *, char *);
void	clean();

#endif
