/*
** send_file.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sat Apr 21 11:31:29 2012 anna texier
** Last update Sun Apr 22 13:19:04 2012 anna texier
*/

#include	"client.h"

void	send_file(t_data *data, t_client *infos)
{
  if (data->magic != MAGIC || data->size_name > NAME_LEN
      || data->size_name != strlen(data->name))
    return;
  if (data->code == ERR_SEND)
    {
      add_to_chat(data->content, ERROR, infos);
      return;
    }
}
