/*
** ptr_func.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Fri Apr 20 15:53:45 2012 anna texier
** Last update Sun Apr 22 20:30:51 2012 anna texier
*/

#include	"client.h"

static void	init_array(t_cmd *array)
{
  array[CMD_NICK].ptr = &change_nickname;
  array[CMD_LIST].ptr = &list_channels;
  array[CMD_JOIN].ptr = &join_channel;
  array[CMD_LEAVE].ptr = &leave_channel;
  array[CMD_USERS].ptr = &list_users;
  array[CMD_MSG].ptr = &send_msg;
  array[CMD_PMSG].ptr = &send_private_msg;
  array[CMD_SEND].ptr = &send_file;
  array[CMD_ACCEPT].ptr = &accept_file;
  array[ERR_NICK].ptr = &change_nickname;
  array[ERR_LIST].ptr = &list_channels;
  array[ERR_JOIN].ptr = &join_channel;
  array[ERR_LEAVE].ptr = &leave_channel;
  array[ERR_USERS].ptr = &list_users;
  array[ERR_MSG].ptr = &send_msg;
  array[ERR_PMSG].ptr = &send_private_msg;
  array[ERR_SEND].ptr = &send_file;
  array[ERR_ACCEPT].ptr = &accept_file;
}

t_cmd		*get_array()
{
  static t_cmd	*array = NULL;

  if (array == NULL)
    {
      array = xmalloc(CMD_NUM * sizeof(*array));
      init_array(array);
    }
  return (array);
}

t_client	*get_infos(t_client *ptr)
{
  static t_client	*infos = NULL;

  if (infos == NULL && ptr != NULL)
    infos = ptr;
  return (infos);
}
