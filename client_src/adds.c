/*
** adds.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Fri Apr 20 10:09:23 2012 anna texier
** Last update Sun Apr 22 22:24:38 2012 anna texier
*/

#include	"client.h"

void		add_fd(t_client *infos, int fd_read, int fd_write, char *file)
{
  t_transfert	*to_add;

  to_add = xmalloc(sizeof(*to_add));
  to_add->read_fd = fd_read;
  to_add->write_fd = fd_write;
  memset(to_add->file, '\0', FILENAME_LEN);
  strncpy(to_add->file, file, FILENAME_LEN);
  infos->fd_list.add(&(infos->fd_list), to_add);
}

int	add_to_chat(char *str, enum e_type type, t_client *infos)
{
  (void) infos;
  if (type == ERROR)
    printf("\033[31m%s\033[0m\n", str);
  else
    printf("\033[32m%s\033[0m\n", str);
  return (0);
}
