/*
** client.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Wed Apr 18 15:27:22 2012 anna texier
** Last update Sun Apr 22 21:38:13 2012 anna texier
*/

#include	"client.h"

static void	init_infos(t_client *infos)
{
  get_array();
  get_infos(infos);
  infos->fd_socket = -1;
  init_list(&(infos->fd_list));
  FD_ZERO(&(infos->readfds));
  FD_ZERO(&(infos->writefds));
  infos->max_fd = 0;
  infos->current_chan = NULL;
  init_list(&(infos->channels));
  atexit(clean);
  memset(infos->nickname, '\0', NAME_LEN);
}

int		main(void)
{
  t_client	infos;
  t_cmd		*array;

  init_infos(&infos);
  exec_client(&infos);
  array = get_array();
  free(array);
  return (0);
}
