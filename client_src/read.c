/*
** read.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Thu Apr 19 18:11:18 2012 anna texier
** Last update Sun Apr 22 22:37:03 2012 anna texier
*/

#include	"client.h"

int		read_client(t_client *infos)
{
  char		buf[CONTENT_LEN + 1];
  ssize_t	ret;

  memset(buf, '\0', CONTENT_LEN);
  if ((ret = read(0, buf, CONTENT_LEN)) <= 0)
    return (die("read", NULL));
  if (strncmp("/server", buf, sizeof("/server") - 1) == 0)
    return (connect_to_server(buf, infos));
  if (infos->fd_socket == -1)
    return (add_to_chat(CONNECT_F, ERROR, infos));
  if (buf[ret - 1] == '\n')
    buf[ret - 1] = '\0';
  return (send_to_server(buf, (infos->current_chan == NULL) ? "" :
			 infos->current_chan, infos));
}

enum e_bool	cmp_fds(t_transfert *to_test, t_transfert *test)
{
  if (to_test->read_fd == test->read_fd &&
      to_test->write_fd == test->write_fd &&
      strncmp(to_test->file, test->file, FILENAME_LEN) == 0)
    return (TRUE);
  return (FALSE);
}

static void	reinit_infos(t_client *infos)
{
  t_list	*tmp;

  close(infos->fd_socket);
  infos->fd_socket = -1;
  memset(infos->nickname, '\0', NAME_LEN);
  tmp = infos->channels.begin;
  while (tmp)
    {
      free(tmp->data);
      tmp = tmp->next;
    }
  infos->channels.delete(&infos->channels);
}

int		read_server(t_client *infos)
{
  ssize_t	ret;
  t_data	data;
  t_cmd		*array;
  t_transfert	test;
  t_transfert	*tmp;

  array = get_array();
  memset(&data, '\0', sizeof(data));
  if ((ret = read(infos->fd_socket, &data, sizeof(data))) <= 0)
    {
      test.read_fd = infos->fd_socket;
      test.write_fd = -1;
      test.file[0] = '\0';
      tmp = infos->fd_list.get(&infos->fd_list, (t_pred) &cmp_fds, &test);
      infos->fd_list.remove(&(infos->fd_list), tmp);
      free(tmp);
      reinit_infos(infos);
      return (add_to_chat(READ_SOCK_ERR, ERROR, infos));
    }
  if (data.code >= 0 && data.code < CMD_NUM)
    (array[data.code].ptr)(&data, infos);
  else
    add_to_chat("Command does not exist.\n", CMD_NUM, infos);
  return (0);
}
