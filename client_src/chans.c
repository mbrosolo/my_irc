/*
** chans.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Sat Apr 21 10:38:27 2012 anna texier
** Last update Sun Apr 22 22:53:05 2012 anna texier
*/

#include	"client.h"

void	change_nickname(t_data *d, t_client *infos)
{
  char	str[sizeof(NICK_CHANGED) + NAME_LEN + 2];

  if (d->magic != MAGIC || d->size_name > NAME_LEN || d->size > CONTENT_LEN
      || d->name[d->size_name] != '\0' || d->content[d->size] != '\0')
    return;
  if (d->code == ERR_NICK)
    {
      add_to_chat(d->content, ERROR, infos);
      return;
    }
  memset(infos->nickname, '\0', NAME_LEN);
  memset(str, '\0', sizeof(NICK_CHANGED) + NAME_LEN);
  strncpy(infos->nickname, d->name, NAME_LEN);
  strncat(str, NICK_CHANGED, sizeof(NICK_CHANGED) + NAME_LEN);
  strncat(str, d->name, sizeof(NICK_CHANGED) + NAME_LEN);
  str[strlen(str)] = '\n';
  add_to_chat(str, CMD_NUM, infos);
}

void	join_channel(t_data *d, t_client *infos)
{
  char	*chan;

  if (d->magic != MAGIC || d->size_name > NAME_LEN || d->size > CONTENT_LEN
      || d->name[d->size_name] != '\0' || d->content[d->size] != '\0')
    return;
  if (d->code == ERR_JOIN)
    {
      add_to_chat(d->content, ERROR, infos);
      return;
    }
  chan = xmalloc((d->size_name + 1) * sizeof(*chan));
  memset(chan, '\0', NAME_LEN);
  strncpy(chan, d->name, d->size_name);
  infos->channels.add(&infos->channels, chan);
  infos->current_chan = chan;
}

enum e_bool	cmp_chans(char *chan, char *test)
{
  if (strncmp(chan, test, NAME_LEN) == 0)
    return (TRUE);
  return (FALSE);
}

void	leave_channel(t_data *d, t_client *infos)
{
  if (d->magic != MAGIC || d->size_name > NAME_LEN || d->size > CONTENT_LEN
      || d->name[d->size_name] != '\0' || d->content[d->size] != '\0'
      || infos->current_chan == NULL
      || strncmp(infos->current_chan, d->name, NAME_LEN) != 0)
    return;
  if (d->code == ERR_LEAVE)
    {
      add_to_chat(d->content, ERROR, infos);
      return;
    }
  infos->channels.remove(&infos->channels, infos->current_chan);
  free(infos->current_chan);
  infos->current_chan = NULL;
}
