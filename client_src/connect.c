/*
** connect.c for my irc in /home/texier_a//projets/irc/my_irc/client_src
** 
** Made by anna texier
** Login   <texier_a@epitech.net>
** 
** Started on  Wed Apr 18 22:43:08 2012 anna texier
** Last update Sun Apr 22 20:57:03 2012 anna texier
*/

#include	<sys/socket.h>
#include	<arpa/inet.h>
#include	<stdlib.h>
#include	<netdb.h>
#include	"client.h"

static int		connect_to_port(char *port_s, struct sockaddr_in *s_in,
					t_client *infos)
{
  unsigned short	port;

  port = (unsigned short) atoi(port_s);
  if (port == 0)
    port = DEFAULT_PORT;
  if (port < 1024)
    return (add_to_chat(PORT_ERROR, ERROR, infos));
  s_in->sin_port = htons(port);
  if (connect(infos->fd_socket, (const struct sockaddr *) s_in,
	      sizeof(*s_in)) != -1)
    {
      add_fd(infos, infos->fd_socket, -1, "");
      add_to_chat(CO_SUCCESS, CMD_NUM, infos);
      return (send_login(infos));
    }
  close(infos->fd_socket);
  infos->fd_socket = -1;
  return (add_to_chat(CO_ERROR, ERROR, infos));
}

int			connect_to_server(char *str, t_client *infos)
{
  struct protoent	*t_proto;
  struct sockaddr_in	s_in;
  char			machine[CONTENT_LEN + 1];
  char			port_s[CONTENT_LEN + 1];
  size_t		i;

  i = pass_command(0, str);
  if (str[i] == '\n')
    return (add_to_chat(SPEC_ADDR, ERROR, infos));
  i = spacecpy(i, machine, str);
  i = pass_command(i, str);
  i = spacecpy(i, port_s, str);
  if ((t_proto = getprotobyname("TCP")) == NULL)
    return (die("getprotobyname", NULL));
  if ((infos->fd_socket = socket(AF_INET, SOCK_STREAM,
				 t_proto->p_proto)) == -1)
    return (add_to_chat(SOCKET_ERROR, ERROR, infos));
  s_in.sin_family = AF_INET;
  s_in.sin_addr.s_addr = inet_addr(machine);
  return (connect_to_port(port_s, &s_in, infos));
}
